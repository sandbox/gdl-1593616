<?php

/**
 * @file
 * Pathauto batch functions for Book Path module
 */

/**
 * Update the URL aliases of all nodes in a book.
 *
 * The aliases need to be updated in order of menu depth so that the aliases
 * of parent nodes are updated before their children.
 */
function book_path_rebuild_batch_process($bid, &$context) {
  if (!isset($context['sandbox']['initialized'])) {
    $context['sandbox']['count'] = 0;
    $context['sandbox']['total'] = db_result(db_query("SELECT COUNT(DISTINCT nid) FROM {book} WHERE bid = %d", $bid));
    $context['sandbox']['depth'] = 1;

    // Get a count of nodes at each menu depth
    foreach (range(1,9) as $depth) {
      $count = db_result(db_query("SELECT COUNT(DISTINCT b.nid) FROM {book} b JOIN {menu_links} ml ON b.mlid = ml.mlid WHERE b.bid = %d AND ml.depth = %d", $bid, $depth));
      if ($count > 0) {
        $context['sandbox']['menu_depth'][$depth]['total'] = $count;
      } else {
        break;
      }
    }
    $context['sandbox']['initialized'] = TRUE;
  }

  $limit = $content['sandbox']['limit'] ? $content['sandbox']['limit'] : 10;

  if (array_key_exists($context['sandbox']['depth'], $context['sandbox']['menu_depth'])) {
    $depth = $context['sandbox']['depth'];
    $count = $context['sandbox']['menu_depth'][$depth]['count'] ? $context['sandbox']['menu_depth'][$depth]['count'] : 0;

    $query = "SELECT b.nid FROM {book} b JOIN {menu_links} ml ON b.mlid = ml.mlid WHERE b.bid=%d AND ml.depth=%d ORDER BY b.nid ASC";
    $result = db_query_range($query, $bid, $depth, $count, $limit);

    $nids = array();
    while ($nid = db_result($result)) {
      $nids[] = $nid;
    }

    if (!empty($nids)) {
      pathauto_node_update_alias_multiple($nids, 'bulkupdate');
      $context['sandbox']['count'] += count($nids);
      $context['sandbox']['menu_depth'][$depth]['count'] += count($nids);
    }

    if ($context['sandbox']['menu_depth'][$depth]['count'] == $context['sandbox']['menu_depth'][$depth]['total']) {
      $context['sandbox']['depth'] += 1;
    }

    $context['message'] = t('Updated alias for node @nid.', array('@nid' => end($nids)));
  }

  if ($context['sandbox']['count'] >= $context['sandbox']['total']) {
    $context['finished'] = 1;
  }
  else {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
  }
}

function book_path_rebuild_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('Updated URL aliases for book.'));
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  }
}
